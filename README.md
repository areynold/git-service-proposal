# Campus Git Service Proposal

**DRAFT**

Notes and talking points to develop a proposal for a campus git service.

Uses [GitPitch](https://gitpitch.com) markdown.

Canonical: https://gitlab.engr.illinois.edu/areynold/campus-git-proposal  
Mirror: https://bitbucket.com/areynold/campus-git-proposal  
Slideshow: https://gitpitch.com/areynold/git-service-proposal/master?grs=bitbucket

**DRAFT**
