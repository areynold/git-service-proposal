# Campus Git Proposal

*Draft*
Talking points to be worked into a proposal for a campus git service.
*Draft*

---

## Who is the audience?

* Only existing git users
    *  Consolidating the “haves”
* All IT Pros
    * Incorporates the “wants”
    * Minimal extra education
* Open access for all campus users
    * Adds opportunity cases
    * Allows need-based adoption by full project teams
    * Significant education requirement

Determines scope and next steps

---

## Next Steps

*(Unsure if these are serial or parallel operations)*

1. Evaluate need for a campus service
    * Establish threshold for sufficient evidence
    * Identify user groups and needs
    * Identify willing service hosts
2. Evaluate options for that service
    * Buy new
    * Scale existing

---

## Benefits of a campus git service

* Institutional control of data
    * Longevity, compliance
* Increased collaboration, transparency, access
    * Innersourcing – culture shift!
* Decreased duplication
    * Of services, of data, of development effort
* Covers all users and usage
    * Research, Instruction, Administration
* Illinois branded

---

## Data about existing campus git usage

(see spreadsheet)

*What other data do we need? How do we get it?*

---

## Options for git server

* Grow existing resources
    * EngrIT seems most likely
* Start a new service
    * Who hosts?

---

## The Big Ask

GitHub for campus (github.illinois.edu)

* Cloud hosted or on premise
* Integration with AD for login and group membership
* Enough storage to hold everything
* GitHub for name recognition, industry leader
* “Nobody got fired for buying IBM”

---

Git facilitator (FTE or partial)

* Ambassador, ombudsman, cheerleader
* “Service manager” of a cloud-hosted appliance
* Handles support, adoption, integration with campus users/units
* Explains what service is, especially if we don’t pick GitHub

---

## Concerns about scaling up existing

What has stopped this from being a campus service so far?

* Why is no one offering or scaling their thing?
* Units aren’t even fully covering all of their users (Tech Services GitHub)

---

“Try what we have and see” vs. “What would we need in 3 years?”

* Onboarding costs for new usage, transition costs
* Really don’t want to change

---

Doing it right

* Compliance, accessibility rules – who makes these decisions?
* Could we start evaluating today?

---

## What are we asking for?

Project can't be entirely grassroots.

* A champion
* Actionable requirements gathering questions
* A promise to act (if we do the needful)

---

## Our questions

* How can we gather missing data to satisfy licensing questions?
* When do we have enough info to satisfy IT Council?
* What do we need to do to trigger a commitment to buy?
* What is the threshold for IT Council action?
* When is the best time to move forward with things?
* Who are we “selling” this to?